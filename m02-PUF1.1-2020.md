# M02 UF1.1

1J/H ISM GRUP B - Dimarts 9 Nov 2020

## Entrega

**I M P O R T A N T**

- **LA PRIMERA PART DE CONVERSIONS DE BASE S'HA DE FER EN PAPER**
- **LA RESTA DE PREGUNTES S'HAN D'ENTREGAR EN FITXER DE TEXT FORMAT MARKDOWN I EXTENSIÓ .md**

## Enunciat

## Conversions de base [4 PUNTS]

1. Converteix els següents codis a les altres bases: 

**ENTREGAR TAMBÉ ELS CÀLCULS**

| DECIMAL | BINARI          | HEXADECIMAL | OCTAL |
| ------- | --------------- | ----------- | ----- |
| 20      |                 |             |       |
|         | 1 0 1 0 1 0 0 1 |             |       |
|         |                 | B1          |       |
|         |                 |             | 127   |
| 116     |                 |             |       |
|         | 1 0 0 1 0 0 0 1 |             |       |
|         |                 | 7           |       |
|         |                 |             | 4     |
| 254     |                 |             |       |
|         |             1 1 |             |       |
|         |                 | EF          |       |
|         |                 |             | 8     |
| 32      |                 |             |       |
|         | 1 1 1 1 1 1 1 1 |             |       |
|         |                 | A6          |       |
|         |                 |             | 27    |

## Conceptes de Sistemes Operatius [4 PUNTS]

1. **Com gestionarà el sistema operatiu l'enviament de dades des de múltiples aplicacions simultàniament per una única interfície, per exemple de xarxa?**  [0,5 punts]

- Resposta: 

2. **De quins mecanismes disposem per a recollir les dades que arriben a un dispositiu de l'ordinador i evitar així que es perdin?**  [0,25 punts]

- Resposta: 

3. **A partir de la següent sortida d'una ordre, responeu les següents preguntes:** [1,5 punts]

```
  1  [      0.0%]    4  [******100.0%]   7  [     0.0%]    10 [     0.0%]
  2  [      0.0%]    5  [        0.0%]   8  [     0.0%]    11 [     0.0%]
  3  [      0.0%]    6  [        0.0%]   9  [     0.0%]    12 [     0.0%]
  Mem[||||||||#*****************************  8.76G/62.8G]   Tasks: 45, 175 thr; 1 running
  Swp[                                           0K/32.0G]   Load average: 0.30 0.25 0.34
                                                             Uptime: 24 days, 02:10:00
    PID USER      PRI  NI  VIRT   RES   SHR S CPU% MEM%   TIME+  Command
      1 root       20   0  167M 12824  8536 S  0.0  0.0  1:30.09 /sbin/init
    415 root20   0  327M  234M  233M S  0.0  0.4  2:23.71 /lib/systemd/systemd-journald
    432 root20   0 21132  5288  4172 S  0.0  0.0  0:01.73 /lib/systemd/systemd-udevd

```

​		A) Quina ordre ens mostrarà aquesta informació?

​				Resposta: 

​		B) Com podem instal.lar aquest aplicatiu de CLI en un sistema operatiu Fedora?

​				Resposta: 

​		C) Quans processadors veu el sistema operatiu?

​				Resposta: 

​		D) Quanta memòria total gestiona el sistema operatiu?

​				Resposta: 

​		E) Quanta memòria RAM està utilitzant actualmente el sistema operatiu?

​				Resposta: 

​		E) Quin és el número de procés per a *systemd-udev*?

​				Resposta:

4. **Què és la memòria 'cache' del sistema operatiu i quina finalitat té?**  [0,25 punts]
    $ free -m
             total        used        free      shared  buff/cache   available
    Mem:           3746        1337         822         263        1587        1819
    Swap:          3875           0        3875

- En quin dispositiu físic de l'ordinador s'emmagatzema la memòria 'cache' en un...?:
  - En un sistema operatiu Linux: 
  - En un sistema operatiu Windows:  


5. **Indica tres sortides d'informació de transaccions:**  [0,75 punts]

- Resposta 1:  
- Resposta 2: 
- Resposta 3: 


6. **Indica tres fonts d'informació de transaccions:** [0,75 punts]

- Resposta 1: 
- Resposta 2: 
- Resposta 3: 




## Ordres [2 PUNTS]

1. **Quina ordre ens permet veure els dispositius d'emmagatzematge en aquest format?** [0,5 punts]
NAME            MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sr0              11:0    1   5,9G  0 rom  /run/media/darta/NOCTURNA_SCN
sda               8:0    0 232,9G  0 disk 
├─sda2            8:2    0 231,9G  0 part 
│ ├─fedora-swap 253:1    0   3,8G  0 lvm  [SWAP]
│ └─fedora-root 253:0    0 228,1G  0 lvm  /
└─sda1            8:1    0     1G  0 part /boot

- Resposta: 

- Quantes particions té el dispositiu sda?: 

2. **Com podem veure l'estat de les interrupcions al sistema operatiu Linux?** [0,5 punts]

- Resposta (Mostreu també les primeres línies de la sortida, no calen totes):


3. **Quina ordre ens permet canviar els permisos del fitxer mostrat per tal que només l'usuari pugui executar-lo?** Nota: Els permisos actuals no s'han de modificar  [0,5 punts]
3826509     4 -rw----r--.  1 darta darta      874  2 oct 16:19 install.sh

- Resposta: 


4. **Sobre propietaris i permisos de fitxers respon:**  [0,5 punts]

   Quins permisos tindrà el grup d'usuaris sobre un fitxer al que apliquem els permisos de fitxer 404?

- Resposta: 

  Com podem canviar el propietari del fitxer install.sh anterior per tal que ara pertanyi a l'usuari 'manolito'?

- Resposta: 





